import Vuex from "vuex";
import Vue from "vue";
import {getData} from "../ajax";
import {formatData,everyNum,all,kong} from "../methods";

Vue.use(Vuex);
export default new Vuex.Store({
    state:{
        List:[],
        num:0,
        sum:0
    },
    getters:{
       selected(state){
           let arr=[];
           state.List.forEach((item,index) => {
               item.spuList.forEach((val,i) => {
                   if(val.number!==0){
                     arr.push(val);
                   }
               })
           })
           return arr.length;
       }
    },
    mutations:{
        update(state,res){
            state.List = res
        },
        operation(state,payload){
            let name = payload.name;
            if(payload.opera == "add"){
                payload.item.number++;
            }else{
                payload.item.number--; 
            }
            //根据name来计算every(左侧的数量)
            everyNum(state.List,name);
            //计算num,sum
            all(state);
        },
        kong(state){
            kong(state.List);
            //num,sum变为0
            state.num = 0;
            state.sum = 0;
        }
    },
    actions:{
        async update(context){
            let res = await getData();
            let result =  formatData(res.data.categoryList);
            console.log(result)
            context.commit("update",result)
        }
    }
})