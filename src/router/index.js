import Vue from 'vue'
import Router from 'vue-router'
import common from "@/components/common";
import mystore from "@/components/mystore";
import dinner from "@/components/dinner";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:"/",
      name:"dinner",
      component:dinner
    },{
      path:"/common",
      name:"common",
      component:common
    },{
      path:"/mystore",
      name:"mystore",
      component:mystore
    }
  ]
})
