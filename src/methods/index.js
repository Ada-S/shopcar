export function formatData(data){
    //给左侧的每一项添加一个every(单个品类的数量)
    //给每一条数据添加一个number(数量)
    data.forEach((item,index) => {
        item.every = 0;
        item.spuList.forEach((val,i) => {
           val.number = 0;
        })
    })
    return data
}
export function everyNum(data,name){
    let num = 0;
    data.forEach((item,index) => {
        if(item.categoryName == name){
            item.spuList.forEach((val,i) => {
                num += val.number;
            })
            item.every = num;
        }
    })
}
export function all(state){
    var [num,sum] = [0,0];
    state.List.forEach((item,index) => {
        num += item.every;
        item.spuList.forEach((val,i) => {
            sum += val.currentPrice * val.number
        })
    })
    state.num = num;
    state.sum = sum;
}
export function kong(data){
    data.forEach((item,index) => {
        item.every = 0;
        item.spuList.forEach((val,i) => {
            val.number = 0;
        })
    })
}